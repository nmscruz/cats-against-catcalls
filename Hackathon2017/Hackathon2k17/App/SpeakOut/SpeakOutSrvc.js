﻿app.factory('SpeakOutSrvc', function ($http) {
    return {
        getData: function () {
            var sampleData = [
                {
                    Image: "someimage.jpg",
                    CodeName: "Anon001",
                    Comment: "A cook trapped me in the cooler and groped me; a customer followed me to the parking and almost wouldn’t let me leave"
                },
                {
                    Image: "someimage.jpg",
                    CodeName: "Anon875",
                    Comment: "At the end of one order I asked if the customer needed anything else and he replied ‘a ****** would be nice."
                },
                {
                    Image: "someimage.jpg",
                    CodeName: "CodenameZero",
                    Comment: "My boss touched me, without my consent"
                }
            ];
            return sampleData;
        }
    }
});