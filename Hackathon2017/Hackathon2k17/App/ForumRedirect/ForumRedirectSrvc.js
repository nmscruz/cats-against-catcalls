﻿app.factory('forumRedirectSrvc', function ($http) {
    return {
        getData: function () {
            var data = [
                {
                    Keywords: ["harrassment", "violence", "abuse", "abused", "stalking", "stalked", "stalk", "harassed", "harass"],
                    Law: "RA 9262 (The Anti-Violence Against Women and Their Children Act of 2004)",
                    Link: ["Muster the confidence to speak out or stand up to the abuser. Though this is very difficult to do, this is basically one of the most essential steps to take. Learn not to be present as a target for the abuse",
                        "Build a trustworthy network of friends, co-workers and neighbors. Tell them that you are suffering from physical abuse, get their commitment to support you and provide assistance when you need it",
                        "Devise an escape plan and put it in place.",
                        "Wear appropriate clothes.",
                        "Watch your drink or ditch your drink",
                        "Keep your online life private",
                        "Don’t trust anyone easily",
                        "Seek professional help. Every state has agencies that handle domestic violence to provide professional, psychological, financial and housing assistance at critical times."]

                },
                {
                    Keywords: ["sexual", "groped"],
                    Law: "RA 7877: Anti-Sexual Harassment Act of 1995",
                    Link: ["Muster the confidence to speak out or stand up to the abuser. Though this is very difficult to do, this is basically one of the most essential steps to take. Learn not to be present as a target for the abuse",
                        "Build a trustworthy network of friends, co-workers and neighbors. Tell them that you are suffering from physical abuse, get their commitment to support you and provide assistance when you need it",
                        "Devise an escape plan and put it in place.",
                        "Wear appropriate clothes.",
                        "Watch your drink or ditch your drink",
                        "Keep your online life private",
                        "Don’t trust anyone easily",
                        "Seek professional help. Every state has agencies that handle domestic violence to provide professional, psychological, financial and housing assistance at critical times."]


                },
                {
                    Keywords: ["discrimination", "gender", "obligations", "equality", "obligation", "belittle"],
                    Law: "RA 9710: Magna Carta of Women",
                    Link: ["Muster the confidence to speak out or stand up to the abuser. Though this is very difficult to do, this is basically one of the most essential steps to take. Learn not to be present as a target for the abuse",
                        "Build a trustworthy network of friends, co-workers and neighbors. Tell them that you are suffering from physical abuse, get their commitment to support you and provide assistance when you need it",
                        "Devise an escape plan and put it in place.",
                        "Wear appropriate clothes.",
                        "Watch your drink or ditch your drink",
                        "Keep your online life private",
                        "Don’t trust anyone easily",
                        "Seek professional help. Every state has agencies that handle domestic violence to provide professional, psychological, financial and housing assistance at critical times."]

                },
                {
                    Keywords: ["raped", "touched", "take advantage"],
                    Law: "RA 8505: Rape Victim Assistance and Protection Act of 1998; RA 8353: Anti-Rape Law of 1997; ",
                    Link: ["Muster the confidence to speak out or stand up to the abuser. Though this is very difficult to do, this is basically one of the most essential steps to take. Learn not to be present as a target for the abuse",
                        "Build a trustworthy network of friends, co-workers and neighbors. Tell them that you are suffering from physical abuse, get their commitment to support you and provide assistance when you need it",
                        "Devise an escape plan and put it in place.",
                        "Wear appropriate clothes.",
                        "Watch your drink or ditch your drink",
                        "Keep your online life private",
                        "Don’t trust anyone easily",
                        "Seek professional help. Every state has agencies that handle domestic violence to provide professional, psychological, financial and housing assistance at critical times."]

                }

            ]
            console.log('service run!' + data);
            return data;
        }
    }
});