var app = angular.module('myApp', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/firstPage', {
            controller: 'firstPageControll',
            templateUrl: 'App/firstPage/firstPage.html'
        })
        .when('/signup', {
            controller: 'Signupctrl',
            templateUrl: 'App/Signup/Signup.html'
	))
        .when('/secondPage', {
            controller: 'secondPageControll',
            templateUrl: 'app/secondPage/secondPage.html'
        })
        .when('/homePage', {
            controller: 'homePageControll',
            templateUrl: 'app/homePage/homePage.html'
        })
        .when('/speakOutPage', {
            controller: 'speakOutPageControll',
            templateUrl: 'app/speakOutPage/speakOutPage.html'
        })
        .when('/hotlinePage', {
            controller: 'hotlinePageControll',
            templateUrl: 'app/hotlinePage/hotlinePage.html'
        })
        .when('/mapPage', {
            controller: 'mapPageControll',
            templateUrl: 'app/mapPage/mapPage.html'
        })
        .otherwise({
            redirectTo: '/firstPage'
        });
})